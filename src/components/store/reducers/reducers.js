import { combineReducers } from 'redux';
import modalWinReducer from './modalWinReducer.js';
import currentEventReducer from './currentEventReducer.js'
import currentEventListReducer from './currentEventListReducer.js'

let combinedReducer=combineReducers({
    windowState: modalWinReducer,
    currentEventState:currentEventReducer,
    currentEventList:currentEventListReducer
});

export default combinedReducer;