import {OPEN_MODAL,CLOSE_MODAL} from '../actions/modalWindowAC.js';

const initState={
    isOpenModal:false,
    editMode:false
}

function modalWinReducer(state=initState,action){
    switch (action.type){
        case OPEN_MODAL:{
            let newState={...state,isOpenModal:true,editMode:action.editMode};
            return newState;
        }
        case CLOSE_MODAL:{
            let newState={...state,isOpenModal:false};
            return newState;
        }
        default:
            return state;
    }
}

export default modalWinReducer;