import {SET_EVENT,CREATE_EMPTY_EVENT} from '../actions/currentEventAC.js';

const initState={
    year:null,
    month:null,
    day:null,
    hour:null,
    minutes:null,
    title:null,
    description:null
}

function currentEventReducer(state=initState,action){
    switch (action.type){
        case SET_EVENT:{
            let newState={...state,
                year:action.newEvent.year,
                month:action.newEvent.month,
                day:action.newEvent.day,
                hour:action.newEvent.hour,
                minutes:action.newEvent.minutes,
                title:action.newEvent.title,
                description:action.newEvent.description
            };
            return newState;
        }
        case CREATE_EMPTY_EVENT:{
            let newState={...state,isOpenModal:false};
            return newState;
        }
        default:
            return state;
    }
}

export default currentEventReducer;