import {READ_EVENT_LIST,CLOSE_INFO_MSG} from '../actions/currentEventListAC.js';

const initState={
    eventList:{},
    isEmptyWin:true,

}

function currentEventListReducer(state=initState,action){
    switch (action.type){
        case READ_EVENT_LIST:{
                let newState={...state,eventList:{}}
                if (action.dtKey in localStorage){
                    let curData=JSON.parse(localStorage[action.dtKey])
                    let isEmpty=false;
                    if (Object.keys(curData).length===0){isEmpty=true}
                    newState={...state,eventList:curData,isEmptyWin:isEmpty}
                    }
                else{
                    newState={...state,eventList:{},isEmptyWin:true}
                }
                return newState;
        }
        case CLOSE_INFO_MSG:{
            let newState={...state,isEmptyWin:false};
            return newState;
        } 
        default:
            return state;
    }
}

export default currentEventListReducer;