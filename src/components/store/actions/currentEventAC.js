const SET_EVENT='SET_EVENT';
const CREATE_EMPTY_EVENT='CLEAR_EVENT';

const set_event=function(newEvent){
    return  {
        type:SET_EVENT,
        newEvent:newEvent
    }
}

const create_empty_event=function(){
    return  {
        type:CREATE_EMPTY_EVENT,
    } 
}

export{
    set_event,SET_EVENT,
    create_empty_event,CREATE_EMPTY_EVENT
}