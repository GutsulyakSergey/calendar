const READ_EVENT_LIST='READ_EVENT_LIST';
const CLOSE_INFO_MSG='CLOSE_INFO_MSG';

const read_event_list=function(dtKey){
    return  {
        type:READ_EVENT_LIST,
        dtKey:dtKey
    }
}
const cloe_info_msg=function(){
    return {
        type:CLOSE_INFO_MSG
    }
}

export{
    read_event_list,READ_EVENT_LIST,
    cloe_info_msg,CLOSE_INFO_MSG
}