const OPEN_MODAL='OPEN_MODAL';
const CLOSE_MODAL='CLOSE_MODAL';

const open_modal=function(editMode){
    return  {
        type:OPEN_MODAL,
        editMode:editMode
    }
}

const close_modal=function(){
    return{
        type:CLOSE_MODAL
    }
}

export{
    open_modal,OPEN_MODAL,
    close_modal,CLOSE_MODAL
}