function dateToKey(dt){
    let year=dt.getFullYear();
    let month=dt.getMonth();
    let day=dt.getDate();
    return `${year}.${month}.${day}`;
}

function getNextDay(year,month,cday){
    let dt=new Date(year*1,month*1,cday*1+1);
    return {year:dt.getFullYear(),
            month:dt.getMonth(),
            date:dt.getDate()
        }
}

function getPrevDay(year,month,day){
    let dt=new Date(year*1,month*1,day*1-1);
    return {year:dt.getFullYear(),
            month:dt.getMonth(),
            date:dt.getDate()
        }
}

function getNextMonth(year,month){
    let dt=new Date(year*1,month*1+1,1);
    return {year:dt.getFullYear(),
            month:dt.getMonth()
        }
}

function getPrevMonth(year,month){
    let dt=new Date(year*1,month*1-1,1);
    return {year:dt.getFullYear(),
            month:dt.getMonth()
        }
}
function getMonthName(monthNum){
    let months=['Январь', 'Феваль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    return months[monthNum];
}
function getWeekDay(dayNum){
    let weekDays=['Воскресение', 'Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'];
    return weekDays[dayNum];
}
function getWeekDayShot(dayNum){
    
    let weekDays=['Вскр', 'Пн','Вт','Ср','Чт','Пт','Сб'];
    return weekDays[dayNum];
}
export {dateToKey,getNextDay,getPrevDay,getNextMonth,getPrevMonth,getMonthName,getWeekDay,getWeekDayShot}