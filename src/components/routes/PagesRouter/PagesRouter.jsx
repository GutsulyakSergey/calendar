import React from 'react';
import {Route} from 'react-router-dom';

import MonthPage from '../../pages/month-page/MonthPage.jsx';
import EventsListPage from '../../pages/events-list-page/EventsListPage.jsx';

class PagesRouter extends React.Component {
          
  render() {

    return (
        <div>
            <Route path="/" exact component={MonthPage} /> 
            <Route path="/calendar/:year/:month" component={MonthPage} />
            <Route path="/events/:year/:month/:day" component={EventsListPage} />
        </div>
    );
  }

}
    
export default PagesRouter;