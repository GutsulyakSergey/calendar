import React from 'react';
import {withRouter} from 'react-router-dom';
// import PropTypes from 'prop-types';
import {dateToKey} from '../../../shared/funs.js';
import './DayCel.scss';
class DayCell extends React.PureComponent {

  // static propTypes = {
  //   curDate:PropTypes.shape({
  //     curYear:PropTypes.number.isRequired,
  //     curMonth:PropTypes.number.isRequired,
  //     curDayOfMonth:PropTypes.number.isRequired,
  //     curDayOfWeek:PropTypes.number.isRequired,
  //   })    
  // };

  state={
    countRecord:null
  }
  goToEventeList=()=>{
    this.props.history.push(`/events/${this.props.curDate.getFullYear()}/${this.props.curDate.getMonth()}/${this.props.curDate.getDate()}/`)
  }  
  isWeekend=()=>{
    let dayOfWeek=this.props.curDate.getDay();
    if ((dayOfWeek===0)||(dayOfWeek===6)) return true
    else return false;
  }
  componentDidMount(){

    let dtKey=dateToKey(this.props.curDate);
    if (dtKey in localStorage){
      let a=localStorage[dateToKey(this.props.curDate)]
      this.countRecord=Object.keys(JSON.parse(a)).length;
    }
    
    this.setState({countRecord:this.countRecord})
  }
  render() {
    let cellClasses="Day-cell"
    if (this.isWeekend()) {
      cellClasses+=" Weekend"
    }
    return (
      <div className={cellClasses} onClick={this.goToEventeList}>
        <div className="Date">{this.props.curDate.getDate()}</div>
        {(this.state.countRecord>0)&&(<div className="Count-records">{this.state.countRecord}</div>)}
      </div>
    )
    ;

  }

}

export default withRouter(DayCell);