import React from 'react';

import DayCell from './day-cell/DayCel.jsx'
import {getMonthName,getNextMonth,getPrevMonth,dateToKey} from '../../shared/funs.js'
import './MonthPage.scss'

class MonthPage extends React.PureComponent {
  
  static propTypes = {
    
  };
  state={
    curMonth:null,
    curYear:null,
    startDay:null
  }
  componentWillReceiveProps(nextProps){
    this.initDate(nextProps);
  }
  componentDidMount(){
    this.initDate(this.props);
  }
  initDate=(curProps)=>{
    let curMonth
    let curYear
    if(typeof curProps.match.params.year == "undefined"){
      let curDate=new Date();
      curMonth=curDate.getMonth();
      curYear=curDate.getFullYear();
    }
    else{
      curMonth=curProps.match.params.month*1;
      curYear=curProps.match.params.year*1;
    }
    let fastDay=new Date(curYear,curMonth,1);
    let fastDayOfWeek=(fastDay.getDay() === 0)?7:fastDay.getDay();
    //Sunday it's 7th day of week
    let startDay=(fastDayOfWeek ===1 )?-6:2-fastDayOfWeek;
    //first week previous month
    this.setState({curMonth:curMonth,curYear:curYear,startDay:startDay});
  }
  setTestData=()=>{
    let testData=require('./test-data.json');
    for(let key in testData){
      localStorage[key]=JSON.stringify(testData[key]);
    }
  }

  goToNextMonth=()=>{
    let nextMonth=getNextMonth(this.state.curYear,this.state.curMonth);
    this.props.history.push(`/calendar/${nextMonth.year}/${nextMonth.month}/`);
  }
  goToPrevMonth=()=>{
    let prevMonth=getPrevMonth(this.state.curYear,this.state.curMonth);
    this.props.history.push(`/calendar/${prevMonth.year}/${prevMonth.month}/`);
  }
  render() {
    let startDay=this.state.startDay;
    
    let dayListElements=[];
    for (let j=0;j<6;j++){
      let curWeek=[]
      for (let weekDay=0;weekDay<7;weekDay++){
        let newDate=new Date(this.state.curYear,this.state.curMonth,startDay)
        let dtKey=dateToKey(newDate)
        curWeek.push(<DayCell curDate={newDate} key={dtKey}/>);
        startDay++;

      }
      let keyWeek=`${this.state.curYear}.${this.state.curMonth}.${j}`;
      dayListElements.push(<div className="Row-week" key={keyWeek}>{curWeek}</div>)
    }
    let dt=new Date(this.state.curYear,this.state.curMonth)
    return (
      <div className="Month-page">
        <div className="Head">
          <div className="Date">
            <span>{getMonthName(dt.getMonth())}, </span>
            <span>{dt.getFullYear()}</span> 
          </div>
          <span>
            <i className="material-icons Btn" onClick={this.goToPrevMonth}>arrow_back_ios</i>
            <i className="material-icons Btn" onClick={this.goToNextMonth}>arrow_forward_ios</i>
          </span>
        </div>
          {dayListElements}
        <input type="button" value="записать тестовые данные" onClick={this.setTestData}/>
      </div>
    )
    ;

  }

}

export default MonthPage;