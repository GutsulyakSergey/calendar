import React from 'react';
import {connect} from 'react-redux';
import {withRouter, NavLink} from 'react-router-dom';

import EventWindow from './event-window/EventWindow.jsx';
import ListRow from './list-row/ListRow.jsx';
import {read_event_list,cloe_info_msg}  from '../../../components/store/actions/currentEventListAC.js'
import {getMonthName,getWeekDay,getNextDay,getPrevDay} from '../../shared/funs.js'

import './EventsListPage.scss'
class EventsListPage extends React.PureComponent {

  static propTypes = {
    
  };
  componentDidMount(){
    let dtKey=`${this.props.match.params.year}.${this.props.match.params.month}.${this.props.match.params.day}`
    this.props.dispatch(read_event_list(dtKey));
  }

  goToNextDay=()=>{
    let nextDay=getNextDay(this.props.match.params.year,this.props.match.params.month,this.props.match.params.day);
    this.props.history.push(`/events/${nextDay.year}/${nextDay.month}/${nextDay.date}/`);
    this.props.dispatch(read_event_list(`${nextDay.year}.${nextDay.month}.${nextDay.date}`));
  }
  goToPrevDay=()=>{
    let prevDay=getPrevDay(this.props.match.params.year,this.props.match.params.month,this.props.match.params.day);
    this.props.history.push(`/events/${prevDay.year}/${prevDay.month}/${prevDay.date}/`);
    this.props.dispatch(read_event_list(`${prevDay.year}.${prevDay.month}.${prevDay.date}`));
  }
  closeInfoMsg=()=>{
    this.props.dispatch(cloe_info_msg());
  }
  render() {
    let lislElements=[]
    for(let i=0;i<24;i++){
      let isData=false;
      let minutes=0;
      let title='';
      let description='';
      if (i in this.props.currentEventList.eventList){
        isData=true;
        minutes=this.props.currentEventList.eventList[i].minutes;
        title=this.props.currentEventList.eventList[i].title;
        description=this.props.currentEventList.eventList[i].description;
      }
      let keyDt=`${this.props.match.params.year}.${this.props.match.params.month}.
      ${this.props.match.params.day}.${i}`
      lislElements.push(<ListRow hour={i}
                          year={this.props.match.params.year}
                          month={this.props.match.params.month}
                          day={this.props.match.params.day}
                          isDataForThisRow={isData}
                          minutes={minutes}
                          title={title}
                          description={description}
                          key={keyDt}       
                        />)
    }

    let dt=new Date(this.props.match.params.year,this.props.match.params.month,this.props.match.params.day)
    return (
      <>
      <div className="Events-list-page">
        <div className="Head">
          <div>
            <div className="Date">
              <NavLink to={`/calendar/${this.props.match.params.year}/${this.props.match.params.month}`}>
                <span>{getMonthName(this.props.match.params.month)} </span>
              </NavLink>
              <span>{dt.getDate()}, </span>
              <span>{dt.getFullYear()}</span> 
            </div>
            <div className="Navigation">
              <span>
                <i className="material-icons Btn" onClick={this.goToPrevDay}>arrow_back_ios</i>
                <i className="material-icons Btn" onClick={this.goToNextDay}>arrow_forward_ios</i>
              </span>
            </div> 
          </div>
          <div className="clearfix">
            <div className="Week">
                {getWeekDay(dt.getDay())}
            </div>
            <div className="Link-curmonth">
              <NavLink to='/'>К календарю на текущий месяц</NavLink>
            </div> 
          </div>
        </div>
        <div className="List-elements">
          {lislElements}
        </div>
        {(this.props.currentEventList.isEmptyWin)&&(
          <div className="Info-msg">
            <p>На этот день собыйтий нет</p>
            <input type="button" value="OK" onClick={this.closeInfoMsg}/>
          </div>
        )}
      </div>
      {(this.props.modalWindowState.isOpenModal)&&(<EventWindow/>)}
      </>
    );

  }

}
const mapStateToProps = function (state) {
  return {
    modalWindowState:state.windowState,
    currentEventList:state.currentEventList
  };
};

export default connect(mapStateToProps)(withRouter(EventsListPage));