import React from 'react';
import {connect} from 'react-redux';

import {read_event_list}  from '../../../../components/store/actions/currentEventListAC.js'
import {open_modal} from '../../../store/actions/modalWindowAC.js'
import {set_event}  from '../../../store/actions/currentEventAC.js'
import './ListRow.scss';


class ListRow extends React.PureComponent {

  static propTypes = {
    
  };
  
  openWindowEdit=(EO)=>{
    this.props.dispatch(set_event(this.setCurrentEventData()));
    this.props.dispatch(open_modal(true));
   }
  openWindowView=(EO)=>{
    if (this.props.isDataForThisRow){
    this.props.dispatch(set_event(this.setCurrentEventData()));
    this.props.dispatch(open_modal(false));
    }
  }
  deleteEvent=()=>{
    let dtKey=`${this.props.year}.${this.props.month}.${this.props.day}`;
    let curData;
    if (dtKey in localStorage){
      curData=JSON.parse(localStorage[dtKey]);
      delete curData[this.props.hour];
    }
    localStorage[dtKey]=JSON.stringify(curData);
    this.props.dispatch(read_event_list(dtKey));
  }
  setCurrentEventData=()=>{
    let curEvent={  year:this.props.year,
                    month:this.props.month,
                    day:this.props.day,
                    hour:this.props.hour,
                    minutes:this.props.minutes,
                    title:this.props.title,
                    description:this.props.description
    }
    return curEvent;
  }
  render() {
    let curDate=new Date();
    let dateForRow=new Date(this.props.year,this.props.month,this.props.day,this.props.hour);
    let clsNmRecord="Record";
    if (this.props.isDataForThisRow){clsNmRecord+=" with-data"}
    return (
      <div className="List-row">
        <div className="Hour">{this.props.hour}</div>
        <div className={clsNmRecord} >
        {(this.props.isDataForThisRow)?
          ( <>
              <div className="Record-body" onClick={this.openWindowView}>
                <div className="Minutes">{this.props.hour}:{this.props.minutes}</div>
                <div className="Record-title">{this.props.title}</div>
              </div>
              <div className="Actions">
                <i className="material-icons Btn" onClick={this.openWindowEdit}>edit</i>
                <i className="material-icons Btn" onClick={this.deleteEvent}>delete</i>
              </div>
            </>
          ):(
            <>
              <div className="Actions">
                {(dateForRow>=curDate)&&(<i className="material-icons Btn" onClick={this.openWindowEdit}>add</i>)}
              </div>
            </>
            )
          }
        </div>
      </div>
    )
    ;
  }

}
const mapStateToProps = function (state) {
  return {
  };
};
export default connect(mapStateToProps)(ListRow);