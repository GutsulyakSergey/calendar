import React from 'react';
import {connect} from 'react-redux';

import {close_modal} from '../../../store/actions/modalWindowAC.js'
import {read_event_list}  from '../../../../components/store/actions/currentEventListAC.js'

import './EventWindow.scss'

class EventWindow extends React.PureComponent {

  static propTypes = {
    
  };
  state={
    isValidMinutes:true,
    validMinutesErr:'',
    isValidTitle:true,
    validTitleErr:'',
    isValidDescription:true,
    validDescriptionErr:'',
    isValidAll:true
  }
  componentDidMount(){
    if(this.props.modalWindowState.editMode){
      this.validAll();
    }
  }
  closeWindow=()=>{
    this.props.dispatch(close_modal());
  }
  validMInutes=()=>{
    let val=this.EditMinutesRef.value;
    if (val>59){
      this.setState({isValidMinutes:false,validMinutesErr:'Число минут должно быть не больше 59',isValidAll:false});
      return
    }
    if (val<0){
      this.setState({isValidMinutes:false,validMinutesErr:'Число минут должно быть положительным',isValidAll:false});
      return
    }
    if (this.state.sValidMinutes!==true){
      this.setState({isValidMinutes:true,validMinutesErr:''});
    }
    
  }
  validTitle=()=>{
    let val=this.EditTitleRef.value;
    if(val.length===0){
      this.setState({isValidTitle:false,validTitleErr:'Заголовок не может быть пустым',isValidAll:false});
      return
    }
    if (this.state.isValidTitle!==true){
      this.setState({isValidTitle:true,validTitleErr:''});
    }
  }
  validDescription=()=>{
    let val=this.EditDescriptionRef.value;
    if((val.length!==0)&&(val.length<6)){
       this.setState({isValidDescription:false,validDescriptionErr:'Длина описания или 0 или больше 5 символов',isValidAll:false});
      return
    }
    if (this.state.isValidDescription!==true){
      this.setState({isValidDescription:true,validDescriptionErr:''});
    }
  }
  validFlags=()=>{
     return this.state.isValidMinutes&&this.state.isValidTitle&&this.state.isValidDescription;
  }
  validAll=()=>{
    this.validTitle();
    this.validTitle();
    this.validDescription();
  }
  saveEvent=()=>{
    if (this.validFlags()){
      let dtKey=`${this.props.currentEvent.year}.${this.props.currentEvent.month}.${this.props.currentEvent.day}`;
      let curRecord={minutes:this.EditMinutesRef.value,
                   title:this.EditTitleRef.value,
                   description:this.EditDescriptionRef.value
    }
    let curData={};
    if (dtKey in localStorage){
      curData=JSON.parse(localStorage[dtKey]);
    }
    curData[this.props.currentEvent.hour]=curRecord;
    localStorage[dtKey]=JSON.stringify(curData);
    this.props.dispatch(read_event_list(dtKey));
    }
  }
  setMinutesRef=(ref)=>{
    this.EditMinutesRef=ref;
  }
  setTitleRef=(ref)=>{
    this.EditTitleRef=ref;
  }
  setDescriptionRef=(ref)=>{
    this.EditDescriptionRef=ref;
  }

  render() {
    return (
      <div className="Event-window">
      {(!this.props.modalWindowState.editMode)?(
        <>
        <div className="Time">
          <i className="material-icons" >access_time</i>
         <span className="Text-time">{this.props.currentEvent.hour}:{this.props.currentEvent.minutes}</span>
        </div>
        <p>Заголовок:</p>
        <p className="Title-record">{this.props.currentEvent.title}</p>
        <p>Описание:</p>
        <p className="Description-record">{this.props.currentEvent.description}</p>
        </>
      ):(
        <>
        
        <div className="Time">
          <i className="material-icons" >access_time</i>
         <span className="Text-time">
         <input type="number" min="0" max="23"
            readOnly={true}
            value={this.props.currentEvent.hour}
          />
          :
          <input type="number" min="0" max="59" onChange={this.validMInutes}
            defaultValue={this.props.currentEvent.minutes}
            ref={this.setMinutesRef}
          />
          </span>
        </div>
        {(!this.state.isValidMinutes)&&(<p className="Err-valid">{this.state.validMinutesErr}</p>)}
        <p>Заголовок:</p>
        <input type="text"
            className="Edit" 
            defaultValue={this.props.currentEvent.title}
            ref={this.setTitleRef}
            onChange={this.validTitle}
          />
        {(!this.state.isValidTitle)&&(<p className="Err-valid">{this.state.validTitleErr}</p>)}
        <p>Описание:</p>
        <textarea 
          className="Edit" 
          defaultValue={this.props.currentEvent.description}
          ref={this.setDescriptionRef}
          onChange={this.validDescription}
          >
        </textarea>
        {(!this.state.isValidDescription)&&(<p className="Err-valid">{this.state.validDescriptionErr}</p>)}

        <div>
          <input type="button" defaultValue="Save" onClick={this.saveEvent}/>
        </div>
        

        </>
        
      )}
        <div className="Btn-close" onClick={this.closeWindow}> <i className="material-icons" >close</i></div>
      </div>
    )
    ;

  }

}
const mapStateToProps = function (state) {
  return {
    modalWindowState:state.windowState,
    currentEvent:state.currentEventState
  };
};
export default connect(mapStateToProps)(EventWindow);
