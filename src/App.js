import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import combinedReducer from './components/store/reducers/reducers.js';
import PagesRouter from './components/routes/PagesRouter/PagesRouter.jsx';

import './App.css';
let store=createStore(combinedReducer);

class App extends Component {
  render() {
    return (
    <Provider store={store}>
      <BrowserRouter>
        <PagesRouter />
      </BrowserRouter>
    </Provider>
    );
  }
}

export default App;
